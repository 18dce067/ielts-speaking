import cv2
from random import randrange
from deepface import DeepFace

trained_face_data = cv2.CascadeClassifier('haarcascade_frontalface_default.xml')

# Choose an image to detect face
#img1 = cv2.imread('Image3.jfif')
webcam = cv2.VideoCapture("Intro.mp4")

# Iterate over frames
while True:
    successful_frame_read, frame = webcam.read()
    result = DeepFace.analyze(frame, actions = ['emotion'])

    # Convert to grey Scale
    grayscaled_img = cv2.cvtColor(frame, cv2.COLOR_BGR2GRAY)

    # Detect Faces
    face_coordinates = trained_face_data.detectMultiScale(grayscaled_img)

    # Draw Rectangle around faces
    for (x, y, w, h) in face_coordinates:
        cv2.rectangle(frame, (x, y), (x+w, y+h), (randrange(256), randrange(256), randrange(256)), 3)

    font = cv2.FONT_HERSHEY_SIMPLEX 

    cv2.putText(frame,
                result['dominant_emotion'],
                (50, 50),
                font, 3,
                (0, 0, 255),
                2,
                cv2.LINE_4)
    

    cv2.imshow('Mayank Parmar Face Detector', frame)
    key = cv2.waitKey(1)

    ### Stop Q key is pressed
    if key == 81 or key == 113:
        break

webcam.release()
print("Code Completed")

""" 

# Detect Faces
face_coordinates = trained_face_data.detectMultiScale(grayscaled_img)
#print(face_coordinates)

# Draw Rectangles
for (x, y, w, h) in face_coordinates:
    cv2.rectangle(img1, (x, y), (x+w, y+h), (randrange(256), randrange(256), randrange(256)), 3)


# Display the images with faces
cv2.imshow('Mayank Parmar Face Detector', img1)
cv2.waitKey()

"""
