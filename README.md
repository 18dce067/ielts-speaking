# IELTS-Speaking

# Installing OpenCV from prebuilt binaries


1. Below Python packages are to be downloaded and installed to their default locations.
    - **Python** 3.x (3.9+) or Python 3.9.x from here.
    - **Numpy package** (for example, using `pip install numpy` command).
    - **Matplotlib** (`pip install matplotlib`) (Matplotlib is optional).
2. Install all packages into their default locations. Python will be installed to C:/Python36/ in case of Python 3.9.
3. After installation, open Python IDLE. Enter `import numpy` and make sure Numpy is working fine.
4. Download latest OpenCV release from `GitHub` or `SourceForge` `site` and double-click to extract it.
5. Goto opencv/build/python/3.9 folder.
6. Copy cv2.pyd to C:/Python39/lib/site-packages.
7. Open Python IDLE and type following codes in Python terminal.
- import cv2 as cv
### If the results are printed out without any errors, congratulations !!! You have installed OpenCV-Python successfully.


